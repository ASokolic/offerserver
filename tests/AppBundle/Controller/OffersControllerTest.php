<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 15/05/2018
 * Time: 09:57
 */

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\OffersController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OffersControllerTest extends WebTestCase
{
    public function testSetOffers(){
        $client = static::createClient();
        $client->request('POST', '/offer');

        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
    }

    public function testGetOffers(){
        $client = static::createClient();
        $client->request('GET', '/offers/1');

        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
    }

    public function testListOffers(){
        $client = static::createClient();
        $client->request('GET', '/offers');

        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
    }
}