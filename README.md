offers_server
=============

A Symfony project created on May 12, 2018, 1:02 pm. by Andrew Sokolic

#requires *PHP 5.5.9*

## Instructions
run the following comands one you have pulled this repo:
```
$ composer install

$ mkdir var/data
$ touch var/data/data.sqlite

$ php bin/console doctrine:schema:update --force
$ php bin/console doctrine:fixtures:load
```

## Serve 
```
php bin/console server:run
```
