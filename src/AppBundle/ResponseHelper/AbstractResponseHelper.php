<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 00:29
 */

namespace AppBundle\ResponseHelper;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AbstractResponseHelper
{

    Protected $Message;
    Protected $Status;
    Protected $Error;

    public function __construct()
    {
        $this->Message = 'Success';
        $this->Status = Response::HTTP_OK;
        $this->Error = null;
    }

    /**
     *
     * @return array
     */
    protected function additionalData(){}

    /**
     * @return array
     */
    protected function additionalHeaders(){}

    public function render(){

        $additionalData = $this->additionalData();

        $data = [
            'Message'=>$this->Message,
            'Status'=>$this->Status,
        ];

        if(!empty($this->Error)){
            $data['Error'] = $this->Error;
        }

        if(!empty($additionalData)){
            $data = array_merge($data,$additionalData);
        }

        return JsonResponse::create($data,$this->Status,$this->additionalHeaders());
    }
}