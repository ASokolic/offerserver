<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 00:39
 */

namespace AppBundle\ResponseHelper;


interface ResponseHelperInterface
{
    public function setMessage($message);
    public function setStatus($status);
    public function setError($error);
    public function additionalData();
    public function additionalHeaders();
//    public function setMessage();
}