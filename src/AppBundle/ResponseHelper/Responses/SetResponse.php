<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 00:44
 */

namespace AppBundle\ResponseHelper\Responses;


use AppBundle\ResponseHelper\AbstractResponseHelper;
use AppBundle\ResponseHelper\ResponseHelperInterface;

class SetResponse extends AbstractResponseHelper implements ResponseHelperInterface
{

    /** @var  integer */
    private $ID;

    public function setID($id)
    {
        $this->ID = $id;
    }

    public function additionalData()
    {
        return ['ID'=>$this->ID];
    }

    public function additionalHeaders()
    {
        return [];
    }

    public function setError($error)
    {
        $this->Error = $error;
    }

    public function setMessage($message)
    {
        $this->Message = $message;
    }

    public function setStatus($status)
    {
        $this->Status = $status;
    }
}