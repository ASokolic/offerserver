<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 14/05/2018
 * Time: 20:57
 */

namespace AppBundle\ResponseHelper\Responses;


use AppBundle\ResponseHelper\AbstractResponseHelper;
use AppBundle\ResponseHelper\ResponseHelperInterface;

class UserResponse extends AbstractResponseHelper implements ResponseHelperInterface
{

    private $User;

    public function setUser($user){
        $this->User = $user;
    }

    public function setMessage($message)
    {
        $this->Message = $message;
    }

    public function setError($error)
    {
        $this->Error = $error;
    }

    public function setStatus($status)
    {
        $this->Status = $status;
    }

    public function additionalHeaders()
    {
        return [];
    }

    public function additionalData()
    {
        return ['User'=>$this->User];
    }
}