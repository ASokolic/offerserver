<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 13/05/2018
 * Time: 01:04
 */

namespace AppBundle\ResponseHelper\Responses;


use AppBundle\ResponseHelper\AbstractResponseHelper;
use AppBundle\ResponseHelper\ResponseHelperInterface;

class GetResponse extends AbstractResponseHelper implements ResponseHelperInterface
{
    private $Data;
    private $DataCount;

    public function setData($data)
    {
        if(is_array($data)) $this->DataCount = count($data);
        $this->Data = $data;
    }

    public function setMessage($message)
    {
        $this->Message = $message;
    }

    public function setError($error)
    {
        // TODO: Implement setError() method.
    }

    public function setStatus($status)
    {
        $this->Status = $status;
    }

    public function additionalHeaders()
    {
        return [];
    }

    public function additionalData()
    {
        $data = [
            'Data'=>$this->Data
        ];

        if(!empty($this->DataCount)) $data['Count'] = $this->DataCount;

        return $data;
    }
}