<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 14/05/2018
 * Time: 19:55
 */

namespace AppBundle\ResponseHelper\Responses;


use AppBundle\ResponseHelper\AbstractResponseHelper;
use AppBundle\ResponseHelper\ResponseHelperInterface;

class DefaultResponse extends AbstractResponseHelper implements ResponseHelperInterface
{

    public function additionalData()
    {
        return [];
    }

    public function additionalHeaders()
    {
        return [];
    }

    public function setError($error)
    {
        $this->Error = $error;
    }

    public function setMessage($message)
    {
        $this->Message = $message;
    }

    public function setStatus($status)
    {
        $this->Status = $status;
    }
}