<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 14/05/2018
 * Time: 20:41
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\ResponseHelper\Responses\UserResponse;
use Doctrine\DBAL\Exception\ServerException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    /**
     * @Route("/user", name="getUser", methods={"POST"})
     */
    public function getUserAction(Request $request)
    {
        $response = new UserResponse();

        $data  = json_decode($request->getContent());

        if(empty($data)){
            $response->setMessage("Missing User details");
            $response->setError(Response::HTTP_BAD_REQUEST);
            $response->setStatus(Response::HTTP_BAD_REQUEST);
            return $response->render();
        }else if(empty($data->Email)){
            $response->setMessage("Missing User Email");
            $response->setError(Response::HTTP_BAD_REQUEST);
            $response->setStatus(Response::HTTP_BAD_REQUEST);
            return $response->render();
        }

        try {
            $user = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->findOneBy(['email' => $data->Email]);

            $response->setUser([
                'Username'=>$user->getUsername(),
                'Email'=>$user->getEmail(),
                'Password'=>$user->getPassword(),
            ]);
        }catch (ServerException $e){
            $response->setMessage('Could not find user.');
            $response->setError('[SQLSTATE]'.$e->getSQLState().'. Violation: '.$e->getErrorCode());
            $response->setStatus(Response::HTTP_NOT_FOUND);
        }

        return $response->render();
    }
}