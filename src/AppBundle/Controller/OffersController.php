<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 12/05/2018
 * Time: 14:16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Offers;
use AppBundle\ResponseHelper\Responses\DefaultResponse;
use AppBundle\ResponseHelper\Responses\GetResponse;
use AppBundle\ResponseHelper\Responses\SetResponse;
use Doctrine\DBAL\Exception\ServerException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OffersController extends Controller
{
    /**
     * @Route("/offer", name="setOffers", methods={"POST","PUT"})
     */
    public function setOffersAction(Request $request)
    {
        $response = new SetResponse();

        $data  = json_decode($request->getContent());

        if(!empty($data->Id)){
            try {
                $offer = $this->getDoctrine()
                    ->getRepository('AppBundle:Offers')
                    ->find($data->Id);
            }catch(ServerException $e){
                $response->setMessage($e->getMessage());
                $response->setError('[SQLSTATE]'.$e->getSQLState().'. Violation: '.$e->getErrorCode());
                $response->setStatus(Response::HTTP_NOT_FOUND);
                return $response->render();
            }
        }else{
            $offer = new Offers();
        }

        $offer->setTitle($data->Title);
        $offer->setDescription($data->Description);
        $offer->setEmail($data->Email);
        $offer->setImageURL($data->ImageURL);
        $offer->setCreateDate(new \DateTime("now"));

        try {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($offer);
            $manager->flush();
        }catch (ServerException $unique){
            $response->setMessage($unique->getMessage());
            $response->setError('[SQLSTATE]'.$unique->getSQLState().'. Violation: '.$unique->getErrorCode());
            $response->setStatus(Response::HTTP_CONFLICT);
        }

        return $response->render();
    }

    /**
     * @Route("/offers", name="listOffers", methods={"GET"})
     */
    public function listOffersAction()
    {
        $response = new GetResponse();

        $offers = $this->getDoctrine()
            ->getRepository('AppBundle:Offers')
            ->findAll();

        if(empty($offers)){
            $response->setMessage("No Offers found");
            $response->setStatus(Response::HTTP_NOT_FOUND);
        }else{

            $records = [];
            /** @var Offers $offer */
            foreach ($offers as $offer){
                $recordObj = new \stdClass();
                $recordObj->Id          = $offer->getId();
                $recordObj->Title       = $offer->getTitle();
                $recordObj->Description = $offer->getDescription();
                $recordObj->Email       = $offer->getEmail();
                $recordObj->ImageURL    = $offer->getImageURL();
                $date = $offer->getCreateDate();
                $recordObj->CreateDate  = $date->format('Y-m-d H:i:s');
                $records[] = $recordObj;
            }
            $response->setData($records);

        }

        return $response->render();
    }

    /**
     * @Route("/offers/{id}", name="fetchOffers", methods={"GET"})
     */
    public function fetchOffersAction($id)
    {
        $response = new GetResponse();

        $offer = $this->getDoctrine()
            ->getRepository('AppBundle:Offers')
            ->find($id);

        $recordObj = new \stdClass();
        $recordObj->Id          = $offer->getId();
        $recordObj->Title       = $offer->getTitle();
        $recordObj->Description = $offer->getDescription();
        $recordObj->Email       = $offer->getEmail();
        $recordObj->ImageURL    = $offer->getImageURL();
        $date = $offer->getCreateDate();
        $recordObj->CreateDate  = $date->format('Y-m-d H:i:s');
        $response->setData($recordObj);

        return $response->render();
    }

    /**
     * @Route("/offers/{id}", name="deleteOffers", methods={"DELETE"})
     */
    public function deleteOffersAction($id){
        $response = new DefaultResponse();

        $offer = $this->getDoctrine()
            ->getRepository('AppBundle:Offers')
            ->find($id);

        try {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($offer);
            $manager->flush();
        }catch (\Exception $unique){
            $response->setMessage($unique->getMessage());
            $response->setError($unique->getCode());
            $response->setStatus(Response::HTTP_BAD_REQUEST);
        }

        return $response->render();
    }
}