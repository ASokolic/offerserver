<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 15/05/2018
 * Time: 00:59
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('Admin');
        $user->setEmail('admin@admin.com');
        $user->setPassword(password_hash('123456', PASSWORD_BCRYPT));

        $manager->persist($user);
        $manager->flush();
    }
}